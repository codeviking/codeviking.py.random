#!/bin/sh
find ./  -name __pycache__ -print0 | xargs -0 rm -rf
find ./  -name '*.pyc' -delete
find ./ -name '*.kate-swp' -delete
find ./ -name '*~' -delete
rm -rf CodeViking.random.egg-info dist build
rm -rf .eggs .pytest_cache .cache
cd docs
make clean
cd ..
