import json, os, sys

SEEDS = [0, 1, 99, 7003]
NUM_SAMPLES = 100
OUT_FILE_NAME = 'test/random-expected.json'

from test.common import generate_samples

from codeviking.random import LCG0, LCG1, LCG2, RNG

def i_range(a, b):
    """
    Create a function f(x) that returns True if x is an integer
    in the range [a,b), or raises a ValueError otherwise
    :type a: int
    :type b: int
    :rtype: (object)->bool
    """
    def __(x):
        if not isinstance(x, int):
            raise ValueError("%s has type %s" % (str(x), type(x).__name__))
        if (a <= x < b):
            return True
        raise ValueError("i_range failed: %d<=%d<%d" % (a, x, b))
    return __


def f_range(a, b):
    """
    Create a function f(x) that returns True if x is a float
    in the range [a,b), or raises a ValueError otherwise
    :type a: float
    :type b: float
    :rtype: (object)->bool
    """

    def __(x):
        if not isinstance(x, float):
            raise ValueError("%s has type %s" % (str(x), type(x).__name__))
        if (a <= x < b):
            return True
        raise ValueError("f_range failed: %f<=%f<%f" % (a, x, b))

    return __


def fa_range(a, _, b):
    """
    Create a function f(x) that returns True if x is a float
    in the range [a,b), or raises a ValueError otherwise
    :type a: float
    :type b: float
    :rtype: (object)->bool
    """

    def __(x):
        if not isinstance(x, float):
            raise ValueError("%s has type %s" % (str(x), type(x).__name__))
        if (a <= x < b):
            return True
        raise ValueError("fa_range failed: %f<=%f<%f" % (a, x, b))

    return __


def i0range(a):
    """
    Create a function f(x) that returns True if x is an integer
    in the range [0,a), or raises a ValueError otherwise
    :type a: int
    :rtype: (object)->bool
    """
    if a<0:
        return i_range(a, 0)
    return i_range(0,a)


def f0range(a):
    """
    Create a function f(x) that returns True if x is a float
    in the range [0,a), or raises a ValueError otherwise
    :type a: float
    :rtype: (object)->bool
    """
    if a < 0.0:
        return f_range(a, 0.0)
    return f_range(0.0, a)


def fn_range(a):
    """
    Create a function f(x) that returns True if x is a float
    in the range [-a,a), or raises a ValueError otherwise
    :type a: float
    :rtype: (object)->bool
    """
    a = abs(a)
    return f_range(-a, a)


def is_in(*items):
    """
    Create a function f(x) that returns True if f is one of items.
    :type items: any*
    :rtype: (object) -> bool
    """
    return lambda x: x in items


def always_true(*_):
    return lambda *_: True


def dice_range(n, d):
    return lambda x: n <= x <= n * d


def is_in_triangle(a, b):
    _x = f0range(a)
    _y = f0range(b)
    slope = -b/a
    _isin = lambda x, y: y <= b + slope * x
    def __(p):
        xl = b + slope * p[0]
        x = _x(p[0])
        y = _y(p[1])
        i = _isin(*p)
        return x and y and i
    return __


FUNC_CALLS = [ ('fr', ((-1.0, 1.0), (2.3, 4.9)), f_range),
               ('ir', ((2, 8), (12, 300), (-5, 0)), i_range),
               ('dice', ((3,5), (7,12), (3,100), (1,32)), dice_range),
               ('i',  (10, 3303, -74), i0range),
               ('f', (1.9, 22.78, -2.4), f0range),
               ('normal', (1.9, 22.78, -2.4), always_true),
               (('f2', 'f3', 'f4', 'f5', 'f6'), (1.0, 23.6, -4.1), fn_range),
               (('f2r', 'f3r', 'f4r', 'f5r', 'f6r'),
                ((-1.0, 1.0), (2.3, 4.9)),
                f_range),
               (('f2a', 'f3a', 'f4a', 'f5a', 'f6a'),
                ((-1.0, 0.2, 1.0), (2.3, 2.7, 4.9)),
                fa_range),
               ('b', is_in(False, True)),
               ('f', f_range(0.0, 1.0)),
               ('sign', is_in(-1, 1)),
               ('normal', (lambda y: True)),
               ('normal', (lambda y: True)),
               ('normal_asym',
                ((1.0, 2.0, 3.0), (-1.7, 1.0, 10.0)),
                always_true),
               ('logistic', ((-1.2,  4.7),(0.1, 8.6),(6.7, 0.9)),
                always_true),
               ('exp', (1.7, -3.8, 1400.1), always_true),
               ('shuffle', (('i', 1000),), always_true),
               ('in_triangle', ((1.0,3.0), (-4.0, 1.0), (-2.0, -3.0),
                                (3.0, -2.0)),
                is_in_triangle)]

def get_funcs(func_or_funcs):
    if isinstance(func_or_funcs, str):
        return [func_or_funcs]
    if isinstance(func_or_funcs, tuple):
        return list(func_or_funcs)
    assert False


def make_func_calls(func_call):
    result = []
    funcs = []
    args_list = [()]
    is_valid = None
    if len(func_call) == 2:
        func_or_funcs, make_is_valid = func_call
        funcs = get_funcs(func_or_funcs)
    else:
        assert(len(func_call)==3)
        func_or_funcs, args_list, make_is_valid = func_call
        funcs = get_funcs(func_or_funcs)

    for fn in funcs:
        for args in args_list:
            if not isinstance(args,tuple):
                args = (args, )
            if len(args)>0:
                is_valid = make_is_valid(*args)
            else:
                is_valid = make_is_valid
            result.append((fn, args, is_valid))
    return result


def call_func(prng, seed, num_samples, fc):
    result = []
    func_name, func_args, is_valid = fc
    rng = RNG(prng(seed))
    samples = generate_samples(rng, func_name, func_args, num_samples)
    for s in samples:
        assert(is_valid(s))
    result.append({'rng_name': prng.__name__,
                   'seed': seed,
                   'num_samples': num_samples,
                   'func_name': func_name,
                   'args': func_args,
                   'expected': samples})
    return result

def generate(rng, seed):
    result = list()
    for func_call in FUNC_CALLS:
        func_calls = make_func_calls(func_call)
        for fc in func_calls:
            result.extend(call_func(rng, seed, NUM_SAMPLES, fc))
    return result

def generate_expected():
    EXP = list()
    for rng in [LCG0, LCG1, LCG2]:
        for seed in SEEDS:
            EXP.extend(generate(rng, seed))
    outf = open(OUT_FILE_NAME, 'w')
    json.dump(EXP, outf, indent=4)

