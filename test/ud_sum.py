import matplotlib.pyplot as plt


def udsum2(n):
    sums = list([0 for i in range(n * 2 + 1)])
    for a1 in range(n + 1):
        for a2 in range(n + 1):
            s = a1 + a2
            sums[s] += 1
    S = sum(sums)
    N = len(sums)
    x = [i / N for i in range(N)]
    y = [s / S for s in sums]
    return x, y


def udsum2s(n):
    sums = list([0 for i in range(n * 3 + 1)])
    for a1 in range(2*n + 1):
        for a2 in range(n + 1):
            s = a1 + a2
            sums[s] += 1
    S = sum(sums)
    N = len(sums)
    x = [i / N for i in range(N)]
    y = [s / S for s in sums]
    return x, y


def udsum3(n):
    sums = list([0 for i in range(n * 3 + 1)])
    for a1 in range(n + 1):
        for a2 in range(n + 1):
            for a3 in range(n + 1):
                s = a1 + a2 + a3
                sums[s] += 1
    S = sum(sums)
    N = len(sums)
    x = [i / N for i in range(N)]
    y = [s / S for s in sums]
    return x, y


def udsum3s(n):
    sums = list([0 for i in range(n * 7 + 1)])
    for a1 in range(4 * n + 1):
        for a2 in range(2 * n + 1):
            for a3 in range(n + 1):
                s = a1 + a2 + a3
                sums[s] += 1
    S = sum(sums)
    N = len(sums)
    x = [i / N for i in range(N)]
    y = [s / S for s in sums]
    return x, y


def udsum4(n):
    sums = list([0 for i in range(n * 4 + 1)])
    for a1 in range(n + 1):
        for a2 in range(n + 1):
            for a3 in range(n + 1):
                for a4 in range(n + 1):
                    s = a1 + a2 + a3 + a4
                    sums[s] += 1
    S = sum(sums)
    N = len(sums)
    x = [i / N for i in range(N)]
    y = [s / S for s in sums]
    return x, y

from math import sqrt, floor
alpha = 0.5
def udsum4s(n):
    mxs=0.0
    sums = list([0 for i in range(n * 10 + 1)])
    for a1 in range(n + 1):
        for a2 in range(n + 1):
            for a3 in range(n + 1):
                for a4 in range(n + 1):
                    s = floor(a1 ** alpha + a2 ** alpha + a3 ** alpha + a4 **
                              alpha)
                    mxs=max(mxs,s)
                    sums[s] += 1
    S = sum(sums)
    N = len(sums)
    x = [i / mxs for i in range(mxs)]
    y = [sums[i] / S for i in range(mxs)]
    return x, y


# x2, y2 = udsum2(100)
# x2s, y2s = udsum2s(33)
# x3, y3 = udsum3(100)
# x3s, y3s = udsum3s(25)
# plt.plot(x3, y3,x3s,y3s)

x4, y4 = udsum4(33)
x4s, y4s = udsum4s(33)
plt.plot(x4, y4,x4s,y4s)
plt.show()
