codeviking.random README
========================

Random number utility functions and generators.  Provides cross-language
random number generator compatibility (the same seed will yield the same
sequence in all supported languages).

This library is avaialable in the following languages:

    Dart:
        Not yet released

    Python:
        https://bitbucket.org/codeviking/python-codeviking.random

    Scala:
        Not yet released



Installation
------------

    pip install codeviking.random

Documentation
-------------

    http://codeviking-random.readthedocs.org/


Support
-------

See the project home page at
https://bitbucket.org/codeviking/python-codeviking.random/
to file a bug report or request a feature.
