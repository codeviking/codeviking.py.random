from codeviking.random import RNG

if __name__ == '__main__':
    import time

    EXP = list()
    N = 1000000
    rng = RNG(0)
    ts1 = time.time()
    for i in range(N):
        a = rng.f3()
    te1 = time.time()
    t = te1 - ts1
    print("f3 x %d: %f:  %f" % (N, t, N / t))

    ts1 = time.time()
    for i in range(N):
        a = rng.normal()
    te1 = time.time()
    t = te1 - ts1
    print("normal x %d: %f:  %f" % (N, t, N / t))



