__author__ = 'dan'


def generate_samples(rng,
                     func_name,
                     args,
                     num_samples):
    if func_name == 'shuffle':
        func_name = args[0]
        f = getattr(rng, func_name)
        args = args[1:]
        v = [f(*args) for i in range(num_samples)]
        rng.shuffle(v)
        return v
    else:
        f = getattr(rng, func_name)
        return [f(*args) for i in range(num_samples)]
