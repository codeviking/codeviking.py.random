import random
import statistics

import matplotlib.pyplot as plt

from codeviking.random import LCG0, RNG


def m2():
    return random.random() - random.random()


def m4():
    return random.random() - random.random() + random.random() - \
           random.random()


def m6():
    return random.random() - random.random() + random.random() - \
           random.random() + random.random() - random.random()


def s2():
    return random.random() + random.random()


def s3():
    return random.random() + random.random() + random.random()


def s4():
    return random.random() + random.random() + random.random() + \
           random.random()


def s5():
    return random.random() + random.random() + random.random() + \
           random.random() + random.random()


def s6():
    return random.random() + random.random() + random.random() + \
           random.random() + random.random() + random.random()


def modified_pert():
    rng = RNG(LCG0(0))
    N = 100000
    samples = [rng.pert(0, 80, 100) for i in range(N)]
    plt.hist(samples, bins=100)
    plt.show()


def run():
    N = 500000
    n = 50
    a2 = [s2() for i in range(N)]
    a3 = [s3() for i in range(N)]
    a4 = [s4() for i in range(N)]
    a5 = [s5() for i in range(N)]
    a6 = [s6() for i in range(N)]

    for i, d in enumerate([a2, a3, a4, a5, a6]):
        m = statistics.mean(d)
        s = statistics.stdev(d, m)
        print(f"{i + 2}: {m}, {s}")

    modified_pert()


if __name__ == "__main__":
    run()
