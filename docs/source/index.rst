CodeViking Random Documentation
===============================


Random number utility functions and generators.  Provides cross-language
random number generator compatibility (the same seed will yield the same
sequence in all supported languages).


This package currently supports Python 3 only.  There is no planned support for
Python 2.  Patches that provide Python 2 compatibility are welcome.


**WARNING**:  Code and documentation are currently in beta testing.
Everything documented here is expected to work, but not all features have
been thoroughly tested yet.  If you find any bugs, please file a bug report.
at https://bitbucket.org/codeviking/python-codeviking.random/

**Sphinx Bug**
The docs for this project can't be built if any other codeviking.* packages are installed.
This is bug in sphinx-apidoc:
https://github.com/sphinx-doc/sphinx/issues/1500
As far as I know, the only workaround is uninstalling any codeviking.* packages before building the docs.



.. toctree::
    :maxdepth: 8


Overview
--------

The main utility class In this package is `random.RNG`.  This
class provides a number of primitives for generating random int, float,
and boolean values, as well as a list/array shuffle method.  RNG does not
actually generate randomnumbers, it requires an instance  of
PrimitiveRandomNumberGenerator.

Users that wish to use a custom random number generator simply need to
subclass PrimitiveRandomNumberGenerator.  Subclasses need to implement the
next_int method, and pass the correct value of int_bound to the
PrimitiveRandomNumberGenerator constructor.


:mod:`codeviking.random` Package
--------------------------------


.. autoclass:: codeviking.random.RNG
   :special-members: __init__
   :members:



Primitive Generators
^^^^^^^^^^^^^^^^^^^^

.. autoclass:: codeviking.random.PrimitiveRandomNumberGenerator
   :special-members: __init__
   :members:

Linear Congruential Generators
""""""""""""""""""""""""""""""

.. autoclass:: codeviking.random.LCG
   :special-members: __init__
   :members:

.. autoclass:: codeviking.random.LCG0
   :special-members: __init__
   :members:

.. autoclass:: codeviking.random.LCG1
   :special-members: __init__
   :members:

.. autoclass:: codeviking.random.LCG2
   :special-members: __init__
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

